import numpy as np 
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt 

data = pd.read_csv('application_train.csv')

missing_data = sns.heatmap(data.isnull(),yticklabels=False,xticklabels=True,cbar=False,cmap='viridis')
chart_fig = missing_data.get_figure()
chart_fig.tight_layout()
chart_fig.savefig('Missing Data')